require 'web_socket_rb'

WebSocketApp = WebSocketRb::App.new

# Define endpoints
WebSocketApp.routes do
  config.port = 9292

  # New connection
  init_connection do
    broadcast_message('calculator', 'Hello!')
  end

  # Close some connection
  close_connection do
    broadcast_message('calculator', 'Good bye')
  end

  # Listen for every new incoming message
  subscribe 'calculator' do |msg|
    send_message('calculator', eval(msg))
  end
end


WebSocketApp.run
