require 'web_socket_rb'

WebSocketApp = WebSocketRb::App.new

# Define endpoints
WebSocketApp.routes do
  config.port = 9292

  # New connection
  init_connection do
    broadcast_message('chat', 'Hello!')
  end

  # Close some connection
  close_connection do
    broadcast_message('chat', 'Good bye')
  end

  # Listen for every new incoming message
  subscribe 'chat' do |msg|
    broadcast_message('chat', msg)
  end
end


WebSocketApp.run
