# WebSocketRb

WebSocketRb it's a simple WebSocket server.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'web_socket_rb'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install web_socket_rb

## Usage

Example of server configuration file:

```ruby
require 'web_socket_rb'

# Create server object
WebSocketApp = WebSocketRb::App.new

# Define endpoints
WebSocketApp.routes do
  config.port = 9292

  # New connection
  init_connection do
    broadcast_message('chat', 'Hello!')
  end

  # Close some connection
  close_connection do
    broadcast_message('chat', 'Good bye')
  end

  # Listen for every new incoming message
  subscribe 'chat' do |msg|
    broadcast_message('chat', msg)
  end
end

# Run server
WebSocketApp.run
```

You can use JS script to make easier integration with server in client side - `js/websocketrb_client.js`. 

You will find more examples in `examples` directory.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

