# Class defines methods available in routes section to configure WebSocket server
module WebSocketRb
  class Routes
    attr_reader :config, :subscribes, :init_connection_code, :close_connection_code

    def initialize
      @subscribes = Hash.new([])
      @config     = OpenStruct.new(port: '9292')
    end

    # Define method to subscribe incoming messages
    def subscribe(name, &block)
      raise ArgumentError, 'Invalid name' unless name.is_a?(String)
      @subscribes[name] = block
    end

    # Execute block of code while initiation new connection
    def init_connection(&block)
      @init_connection_code = block
    end

    # Execute this block of code when connection is closed
    def close_connection(&block)
      @close_connection_code = block
    end
  end
end
