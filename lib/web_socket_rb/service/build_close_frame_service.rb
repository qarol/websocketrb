require 'web_socket_rb/wrapper/frame_close'

module WebSocketRb
  module Service
    class BuildCloseFrameService
      def initialize(close_frame)
        @close_frame = close_frame
      end

      def run
        WebSocketRb::Wrapper::FrameClose.new
      end
    end
  end
end
