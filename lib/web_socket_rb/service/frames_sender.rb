module WebSocketRb
  module Service
    class FramesSender
      attr_reader :connections

      def initialize(connections, connection)
        @connections         = connections
        @connection          = connection
        @frames_to_send      = []
        @frames_to_broadcast = []
        Thread.new do
          loop do
            frame = @frames_to_send.shift
            send_frame(frame) unless frame.nil?
            frame = @frames_to_broadcast.shift
            broadcast_frame(frame) unless frame.nil?
          end
        end
      end

      # Method to send frame in current connection
      def frame_to_send(frame)
        @frames_to_send << frame
      end

      # Method to send frame in all connections
      def frame_to_broadcast(frame)
        @frames_to_broadcast << frame
      end

      private

      # Broadcast message through current connection
      def send_frame(frame)
        @connection.write(frame.to_bytes)
        Thread.close if frame.close?
      end

      # Broadcast message through all connections
      def broadcast_frame(frame)
        @connections.each do |conn|
          conn.write(frame.to_bytes)
        end
      end
    end
  end
end
