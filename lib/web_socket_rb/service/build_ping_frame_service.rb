require 'web_socket_rb/wrapper/frame_ping'

module WebSocketRb
  module Service
    class BuildPingFrameService
      def run
        WebSocketRb::Wrapper::FramePing.new
      end
    end
  end
end
