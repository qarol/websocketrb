require 'web_socket_rb/wrapper/frame_text'

module WebSocketRb
  module Service
    class BuildTextFrameService
      def initialize(destination, message)
        @destination = destination
        @message     = message
      end

      def run
        payload_data = JSON.generate(destination: @destination, message: @message)
        WebSocketRb::Wrapper::FrameText.new(payload_data)
      end
    end
  end
end
