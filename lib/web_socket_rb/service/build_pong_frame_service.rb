require 'web_socket_rb/wrapper/frame_pong'

# Create PONG frame based of PING request.
module WebSocketRb
  module Service
    class BuildPongFrameService
      def initialize(ping_frame)
        @ping_frame = ping_frame
      end

      # Every PONG frame should have the same payload_data as PING frame
      def run
        pong_frame              = WebSocketRb::Wrapper::FramePong.new
        pong_frame.payload_data = @ping_frame.payload_data
        pong_frame
      end
    end
  end
end
