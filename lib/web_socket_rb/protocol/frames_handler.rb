require 'web_socket_rb/context/sandbox'
require 'web_socket_rb/service/frames_sender'
require 'web_socket_rb/service/read_frame_service'
require 'web_socket_rb/service/build_ping_frame_service'
require 'web_socket_rb/service/build_pong_frame_service'
require 'web_socket_rb/service/build_close_frame_service'
require 'web_socket_rb/error/wrong_frame_error'

module WebSocketRb
  module Protocol
    class FramesHandler
      def initialize(connections, connection, routes, frames_sender, sandbox)
        @connections      = connections
        @conn             = connection
        @routes           = routes
        @frames_sender    = frames_sender
        @sandbox          = sandbox
        @threads          = []
        @pending_pings    = []
        @mutex            = Mutex.new
      end

      # Run reading and sending frames
      def run
        @threads << Thread.new { verify_status }
        @threads << Thread.new { process_incoming_frames }
        @threads.each(&:join)
      end

      private

      # Send PING frame every 10 seconds to verify connection
      def verify_status
        loop do
          frame = Service::BuildPingFrameService.new.run
          @frames_sender.frame_to_send(frame)
          @mutex.synchronize { @pending_pings << frame }
          sleep(10)
          @mutex.synchronize { @threads.reverse.each(&:kill) if @pending_pings.include?(frame) }
        end
      end

      # Read incoming frames
      def process_incoming_frames
        loop do
          frame = Service::ReadFrameService.new(@conn).run
          process_incoming_frame(frame) unless frame.nil?
        end
      end

      # Process incoming frame
      def process_incoming_frame(frame)
        if frame.ping?

          # Send PONG frame if requested PING frame
          App.logger.info('Frames handler') { 'Replying for PING request' }
          pong_frame = Service::BuildPongFrameService.new(frame).run
          @frames_sender.frame_to_send(pong_frame)

        elsif frame.close?

          # Send CLOSE frame if requested CLOSE frame
          App.logger.info('Frames handler') { 'Replying for CLOSE request' }
          close_frame = Service::BuildCloseFrameService.new(frame).run
          @frames_sender.frame_to_send(close_frame)
          @threads.each(&:exit)

        elsif frame.pong?

          # Verify pending PINGs and find one with the same payload data
          App.logger.info('Frames handler') {'Verifying PINGS'}
          @mutex.synchronize do
            pending_ping = @pending_pings.reject! do |ping_frame|
              frame.payload_data == ping_frame.payload_data
            end
            @threads.each(&:exit) if pending_ping.nil?
          end

        elsif frame.text? || frame.binary?

          # Call subscribe block
          App.logger.info('Frames handler') { 'Processing frame' }
          subscribe_block = @routes.subscribes[frame.destination]
          App.logger.info('Frames handler') { frame.message }
          Thread.new { @sandbox.instance_exec(frame.message, &subscribe_block) }

        end
      end
    end
  end
end
