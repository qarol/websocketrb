require 'web_socket_rb/error/handshake_error'
require 'web_socket_rb/wrapper/handshake_request'
require 'digest'

module WebSocketRb
  module Protocol
    class Handshake
      MAGIC_KEY = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'.freeze

      def initialize(conn)
        @conn = conn
      end

      # Generate a proper handshake response based on request
      def run
        validate_request
        generate_proper_response
        @conn
      end

      private

      # Read incoming data from socket and convert it to handshake request
      def request
        @request ||= Wrapper::HandshakeRequest.new(@conn)
      end

      # Validate incoming request - is it a WebSocket upgrade request?
      def validate_request
        App.logger.info('Handshake') { 'Verification started' }
        raise Error::HandshakeError, 'It is not an upgrade request' unless request.upgrade?
        App.logger.info('Handshake') { 'Valid upgrade request' }
        raise Error::HandshakeError, 'Not valid protocol version.' unless request.valid_version?
        App.logger.info('Handshake') { 'Valid protocol version' }
        raise Error::HandshakeError, 'Not valid sub-protocol. Only JSON available.' unless request.valid_protocol?
        App.logger.info('Handshake') { 'Valid sub-protocol request' }
        App.logger.info('Handshake') { 'Verification passed' }
      end

      # Generate a proper response based on request
      def generate_proper_response
        App.logger.info('Handshake') { 'Send accept response' }
        @conn.puts('HTTP/1.1 101 Switching Protocols')
        @conn.puts('Upgrade: websocket')
        @conn.puts('Connection: Upgrade')
        @conn.puts("Sec-WebSocket-Accept: #{calculate_key(request.key)}")
        @conn.puts('')
        App.logger.info('Handshake') { 'Finished sending accept response' }
      end

      # Calculate handshake key
      def calculate_key(key)
        raise ArgumentError, 'Key must be a String' unless key.is_a?(String)
        ::Digest::SHA1.base64digest(key.strip + MAGIC_KEY)
      end
    end
  end
end
