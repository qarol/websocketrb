require 'web_socket_rb/wrapper/frame_outgoing'

# Class describes Pong frame - reply to Ping frame
module WebSocketRb
  module Wrapper
    class FramePong < FrameOutgoing
      def initialize
        super(opcode: PONG)
      end
    end
  end
end
