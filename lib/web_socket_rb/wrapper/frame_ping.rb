require 'web_socket_rb/wrapper/frame_outgoing'

# Class describes Ping frame - to verify connection status.
module WebSocketRb
  module Wrapper
    class FramePing < FrameOutgoing
      def initialize
        rand_payload_data = Random.new.bytes(10)
        super(opcode: PING, payload_data: rand_payload_data)
      end
    end
  end
end
