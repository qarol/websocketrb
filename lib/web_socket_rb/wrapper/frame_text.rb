require 'web_socket_rb/wrapper/frame_outgoing'
require 'json'

# Class describes Closing frame
module WebSocketRb
  module Wrapper
    class FrameText < FrameOutgoing
      def initialize(payload_data)
        super(opcode: TEXT, payload_data: payload_data)
      end
    end
  end
end
