require 'web_socket_rb/wrapper/frame_outgoing'

# Class describes Closing frame
module WebSocketRb
  module Wrapper
    class FrameClose < FrameOutgoing
      def initialize
        super(opcode: CLOSE)
      end
    end
  end
end
