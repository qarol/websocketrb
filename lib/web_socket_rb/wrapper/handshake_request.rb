# Wrapper for request to get.
# It extracts a proper information from request header, like:
# - requested path
# - http protocol version
# - list of header key-val pairs
module WebSocketRb
  module Wrapper
    class HandshakeRequest
      HTTP_HEADER = /^GET (\/[[:graph:]]*[\/[[:graph:]]]*) HTTP\/([[:digit:]]+\.[[:digit:]]+)$/ # GET /chat HTTP/1.1
      VERSION     = 13
      PROTOCOL    = 'json'.freeze

      attr_reader :path, :version, :headers

      def initialize(request)
        @request = request
        @headers = {}
        @version = '1.1'
        @path    = '/'

        convert_request_to_hash
      end

      # Verify if request contains upgrade to websocket demand
      def upgrade?
        headers['Connection'] == 'Upgrade' && headers['Upgrade'] == 'websocket'
      end

      # Verify if request contains a valid version of websocket protocol
      def valid_version?
        headers['Sec-WebSocket-Version'].to_i == VERSION
      end

      # Verify if request contains a valid protocol request
      def valid_protocol?
        protocols = headers['Sec-WebSocket-Protocol'].to_s
        protocols = protocols.split(',').map(&:strip)
        protocols.empty? || protocols.include?(PROTOCOL)
      end

      def key
        headers['Sec-WebSocket-Key'].to_s
      end

      private

      # Read each line of request and store it into Hash
      # as key-val pair of all parsed headers
      def convert_request_to_hash
        until (line = @request.gets.strip).empty?
          if !!(line =~ HTTP_HEADER)
            _, @path, @version = line.match(HTTP_HEADER).to_a
          else
            key, val = line.split(':', 2).map(&:strip)
            @headers.store(key, val)
          end
        end
      end
    end
  end
end
