module WebSocketRb
  module Wrapper
    # Class defines base frame structure.
    class FrameBase
      # Constants represents type of frame
      CONTINUATION = 0x0.to_i
      TEXT         = 0x1.to_i
      BINARY       = 0x2.to_i
      CLOSE        = 0x8.to_i
      PING         = 0x9.to_i
      PONG         = 0xA.to_i

      attr_accessor :fin, :rsv1, :rsv2, :rsv3, :opcode, :mask, :payload_len,
                    :masking_key, :payload_data

      # Method converts frame to bytes representation.
      def to_bytes
        bytes = ''
        bytes << fin_rsv_opcode_to_byte
        bytes << mask_payload_length_to_byte
        bytes << ext_payload_len_to_byte
        bytes << masking_key_to_byte
        bytes << payload_data_to_byte
        bytes
      end

      # Define methods to verify types of frame
      [:continuation, :text, :binary, :close, :ping, :pong].each do |constant|
        define_method "#{constant}?" do
          opcode.eql?(self.class.const_get(constant.upcase))
        end
      end

      # Read destination from payload data
      def destination
        json_payload_data = JSON.parse(payload_data)
        json_payload_data.is_a?(Hash) ? json_payload_data['destination'] : nil
      end

      # Read message from payload data
      def message
        json_payload_data = JSON.parse(payload_data)
        json_payload_data.is_a?(Hash) ? json_payload_data['message'] : nil
      end

      protected

      def payload_data_to_byte
        if mask
          payload_data.bytes.each_slice(4).map do |slice|
            slice.zip(masking_key.bytes).map { |a, b| a ^ b }
          end.flatten.pack('C*')
        else
          payload_data.bytes.pack('C*')
        end
      end

      def masking_key_to_byte
        mask && masking_key ? masking_key.bytes.pack('C*') : ''
      end

      def ext_payload_len_to_byte
        case payload_len
        when 0..125
          ''
        when 126..65_535
          [payload_len].pack('S>')
        when 65_536..18_446_744_073_709_551_615
          [payload_len].pack('Q>')
        end
      end

      def mask_payload_length_to_byte
        byte = 0
        byte |= 1 if mask
        byte <<= 7

        case payload_len
        when 0..125
          byte |= payload_len
        when 126..65_535
          byte |= 126
        when 65_536..18_446_744_073_709_551_615
          byte |= 127
        end

        byte.chr
      end

      def fin_rsv_opcode_to_byte
        byte = 0
        byte |= 1 if fin

        byte <<= 1
        byte |= 1 if rsv1

        byte <<= 1
        byte |= 1 if rsv2

        byte <<= 1
        byte |= 1 if rsv3

        byte <<= 4
        byte |= opcode

        byte.chr
      end
    end
  end
end
