require 'web_socket_rb/wrapper/frame_base'

# Class describes any sends frame. It accepts message
module WebSocketRb
  module Wrapper
    class FrameOutgoing < FrameBase
      def initialize(options = {})
        @payload_data = options.fetch(:payload_data, '')
        @payload_len  = @payload_data.bytes.size
        @fin          = options.fetch(:fin, true)
        @rsv1         = options.fetch(:rsv1, false) # No externals (false by default)
        @rsv2         = options.fetch(:rsv2, false) # No externals (false by default)
        @rsv3         = options.fetch(:rsv3, false) # No externals (false by default)
        @opcode       = options.fetch(:opcode, CONTINUATION) # Type of frame (continuation by default)
        @mask         = options.fetch(:mask, false) # Mask (false by default)
        @masking_key  = Random.new.bytes(4) if @mask # Generate random masking key if mask
      end
    end
  end
end
