module WebSocketRb
  module Error
    class WrongFrameError < StandardError
      def initialize(msg)
        App.logger.error('Frame') { msg }
      end
    end
  end
end
