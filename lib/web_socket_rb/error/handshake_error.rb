module WebSocketRb
  module Error
    class HandshakeError < StandardError
      def initialize(msg)
        App.logger.error('Handshake') { msg }
      end

      def messages(conn)
        conn.puts('HTTP/1.1 400 Bad Request')
        conn.puts('Upgrade: websocket')
        conn.puts('Connection: Upgrade')
        conn.puts('Sec-WebSocket-Version: 13')
        conn.puts('Sec-WebSocket-Protocol: json')
      end
    end
  end
end
