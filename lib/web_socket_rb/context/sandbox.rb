require 'web_socket_rb/service/build_text_frame_service'

# This class provides methods available inside of blocks executed in routes
module WebSocketRb
  module Context
    class Sandbox
      def initialize(frames_sender)
        @frames_sender = frames_sender
      end

      # Method to get count of current connections
      def connections_size
        @frames_sender.connections.size
      end

      # Method to send message to client
      def send_message(destination, message)
        frame = Service::BuildTextFrameService.new(destination, message).run
        @frames_sender.frame_to_send(frame)
      end

      # Method to broadcast message to all clients
      def broadcast_message(destination, message)
        frame = Service::BuildTextFrameService.new(destination, message).run
        @frames_sender.frame_to_broadcast(frame)
      end
    end
  end
end
