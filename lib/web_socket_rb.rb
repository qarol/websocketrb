require 'web_socket_rb/version'
require 'web_socket_rb/server'
require 'web_socket_rb/routes'

module WebSocketRb
  class App
    @logger = Logger.new(STDOUT)

    def initialize
      @routes = WebSocketRb::Routes.new
      @server = WebSocketRb::Server.new(@routes)
    end

    def routes(&block)
      @routes.instance_eval(&block)
    end

    # Run WebSocketRb server
    def run
      @server.run
    end

    class << self
      attr_reader :logger
    end
  end
end
