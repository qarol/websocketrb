/**
 * Created by Karol Bajko
 * Client library for WebSocketRb
 */

var WebSocketRbClient = (function (url, protocols) {

    var url = url;
    var protocols = protocols;
    var websocket = null;
    var callbacks = {};

    this.send = function (destination, message) {
        websocket.send(JSON.stringify({destination: destination, message: message}));
        return this;
    };

    this.close = function (code, reason) {
        websocket.close(code, reason);
        return this;
    };

    this.onmessage = function (destination, callback) {
        callbacks[destination] = callbacks[destination] || [];
        callbacks[destination].push(callback)
    };

    this.connect = function () {
        websocket = new WebSocket(url, protocols);

        websocket.onmessage = function (event) {
            var incomingData = JSON.parse(event.data);
            var destination = incomingData.destination;
            var message = incomingData.message;

            var chain = callbacks[destination];
            if (typeof chain == 'undefined') return;
            for (var i = 0; i < chain.length; i++) {
                chain[i](message)
            }
        }
    }
});
