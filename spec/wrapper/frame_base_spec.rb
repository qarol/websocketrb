require 'spec_helper'

describe WebSocketRb::Wrapper::FrameBase do
  subject { described_class.new }

  describe '#fin_rsv_opcode_to_byte' do
    context 'when basic attributes are filled' do
      shared_examples 'first_byte' do |fin, rsv1, rsv2, rsv3, opcode, result|
        it do
          allow(subject).to receive(:fin) { fin }
          allow(subject).to receive(:rsv1) { rsv1 }
          allow(subject).to receive(:rsv2) { rsv2 }
          allow(subject).to receive(:rsv3) { rsv3 }
          allow(subject).to receive(:opcode) { opcode }
          expect(subject.send(:fin_rsv_opcode_to_byte).bytes).to eq(result.chr.bytes)
        end
      end

      include_examples('first_byte', true, false, false, false, described_class::CONTINUATION, 128)
      include_examples('first_byte', true, false, false, false, described_class::TEXT, 129)
      include_examples('first_byte', true, false, false, false, described_class::BINARY, 130)
      include_examples('first_byte', true, false, false, false, described_class::CLOSE, 136)
      include_examples('first_byte', true, false, false, false, described_class::PING, 137)
      include_examples('first_byte', true, false, false, false, described_class::PONG, 138)
      include_examples('first_byte', false, false, false, false, described_class::CONTINUATION, 0)
      include_examples('first_byte', false, false, false, false, described_class::TEXT, 1)
      include_examples('first_byte', false, false, false, false, described_class::BINARY, 2)
      include_examples('first_byte', false, false, false, false, described_class::CLOSE, 8)
      include_examples('first_byte', false, false, false, false, described_class::PING, 9)
      include_examples('first_byte', false, false, false, false, described_class::PONG, 10)
    end
  end

  describe '#mask_payload_length_to_byte' do
    context 'when basic attributes are filled' do
      shared_examples 'second_byte' do |mask, payload_len, result|
        it do
          allow(subject).to receive(:mask) { mask }
          allow(subject).to receive(:payload_len) { payload_len }
          expect(subject.send(:mask_payload_length_to_byte).bytes).to eq(result.chr.bytes)
        end
      end

      include_examples('second_byte', true, 0, 128)
      include_examples('second_byte', true, 56, 184)
      include_examples('second_byte', true, 125, 253)
      include_examples('second_byte', true, 126, 254)
      include_examples('second_byte', true, 127, 254)
      include_examples('second_byte', true, 65_570, 255)
    end
  end

  describe '#ext_payload_len_to_byte' do
    context 'when basic attributes are filled' do
      shared_examples 'ext_payload_len' do |payload_len, result|
        it do
          allow(subject).to receive(:payload_len) { payload_len }
          expect(subject.send(:ext_payload_len_to_byte).bytes).to eq(result.bytes)
        end
      end

      include_examples('ext_payload_len', 0, '')
      include_examples('ext_payload_len', 125, '')
      include_examples('ext_payload_len', 126, "\x00~")
      include_examples('ext_payload_len', 65_536, "\x00\x00\x00\x00\x00\x01\x00\x00")
    end
  end

  describe '#masking_key_to_byte' do
    context 'when basic attributes are filled' do
      shared_examples 'masking_key' do |mask, masking_key, result|
        it do
          allow(subject).to receive(:mask) { mask }
          allow(subject).to receive(:masking_key) { masking_key }
          expect(subject.send(:masking_key_to_byte).bytes).to eq(result.bytes)
        end
      end

      include_examples('masking_key', true, "\xcb\x30\xbf\xea", "\xcb\x30\xbf\xea")
      include_examples('masking_key', false, "\xcb\x30\xbf\xea", '')
      include_examples('masking_key', true, nil, '')
      include_examples('masking_key', false, nil, '')
    end
  end

  describe '#payload_data_to_byte' do
    context 'when basic attributes are filled' do
      shared_examples 'payload_data' do |mask, masking_key, payload_data, result|
        it do
          allow(subject).to receive(:mask) { mask }
          allow(subject).to receive(:masking_key) { masking_key }
          allow(subject).to receive(:payload_data) { payload_data }
          expect(subject.send(:payload_data_to_byte).bytes).to eq(result.bytes)
        end
      end

      include_examples('payload_data', true, "\xcb\x30\xbf\xea", 'This is a random text',
                       "\x9f\x58\xd6\x99\xeb\x59\xcc\xca\xaa\x10\xcd\x8b\xa5\x54\xd0\x87\xeb\x44\xda\x92\xbf")
    end
  end

  describe '#to_bytes' do
    context 'when all attributes are filled' do
      before do
        allow(subject).to receive(:fin) { true }
        allow(subject).to receive(:rsv1) { false }
        allow(subject).to receive(:rsv2) { false }
        allow(subject).to receive(:rsv3) { false }
        allow(subject).to receive(:opcode) { described_class::TEXT }
        allow(subject).to receive(:mask) { true }
        allow(subject).to receive(:payload_len) { 21 }
        allow(subject).to receive(:masking_key) { "\xcb\x30\xbf\xea" }
        allow(subject).to receive(:payload_data) { 'This is a random text' }
      end

      it 'creates a valid bytes string' do
        expect(subject.to_bytes.bytes).to(
          eq("\x81\x95\xcb\x30\xbf\xea\x9f\x58\xd6\x99\xeb\x59\xcc\xca\xaa\x10\xcd\x8b\xa5\x54\xd0\x87\xeb\x44\xda\x92\xbf".bytes)
        )
      end
    end
  end

  describe '#continuation?' do
    context 'when opcode is continuation' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::CONTINUATION }
        expect(subject.continuation?).to be_truthy
      end
    end
    context 'when opcode is not continuation' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.continuation?).to be_falsey
      end
    end
  end

  describe '#text?' do
    context 'when opcode is text' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::TEXT }
        expect(subject.text?).to be_truthy
      end
    end
    context 'when opcode is not text' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.text?).to be_falsey
      end
    end
  end

  describe '#binary?' do
    context 'when opcode is binary' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::BINARY }
        expect(subject.binary?).to be_truthy
      end
    end
    context 'when opcode is not binary' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.binary?).to be_falsey
      end
    end
  end

  describe '#close?' do
    context 'when opcode is close' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::CLOSE }
        expect(subject.close?).to be_truthy
      end
    end
    context 'when opcode is not close' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.close?).to be_falsey
      end
    end
  end

  describe '#ping?' do
    context 'when opcode is ping' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::PING }
        expect(subject.ping?).to be_truthy
      end
    end
    context 'when opcode is not ping' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.ping?).to be_falsey
      end
    end
  end

  describe '#pong?' do
    context 'when opcode is pong' do
      it 'should return a true' do
        allow(subject).to receive(:opcode) { described_class::PONG }
        expect(subject.pong?).to be_truthy
      end
    end
    context 'when opcode is not pong' do
      it 'should return a false' do
        allow(subject).to receive(:opcode) { nil }
        expect(subject.pong?).to be_falsey
      end
    end
  end
end
