require 'spec_helper'

describe WebSocketRb::Wrapper::HandshakeRequest do
  let(:request) { double('request') }
  subject { described_class.new(request) }

  context 'when received a valid handshake request' do
    before do
      allow(request).to receive(:gets).and_return("GET /chat HTTP/1.2\r\n",
                                                  "Host: server.example.com\r\n",
                                                  "Upgrade: websocket\r\n",
                                                  "Connection: Upgrade\r\n",
                                                  "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==\r\n",
                                                  "Origin: http://example.com\r\n",
                                                  "Sec-WebSocket-Protocol: json, chat\r\n",
                                                  "Sec-WebSocket-Version: 13\r\n",
                                                  '')
    end

    describe '#convert_request_to_hash' do
      it 'should read data from stream and return a proper structure of data' do
        expect(subject.path).to eq('/chat')
        expect(subject.version).to eq('1.2')
        expect(subject.headers).to include('Host'                   => 'server.example.com',
                                           'Upgrade'                => 'websocket',
                                           'Connection'             => 'Upgrade',
                                           'Sec-WebSocket-Key'      => 'dGhlIHNhbXBsZSBub25jZQ==',
                                           'Origin'                 => 'http://example.com',
                                           'Sec-WebSocket-Protocol' => 'json, chat',
                                           'Sec-WebSocket-Version'  => '13')
      end
    end

    describe '#upgrade?' do
      it { expect(subject.upgrade?).to be_truthy }
    end

    describe '#valid_version?' do
      it { expect(subject.valid_version?).to be_truthy }
    end

    describe '#valid_protocol??' do
      it { expect(subject.valid_protocol?).to be_truthy }
    end
  end
end
