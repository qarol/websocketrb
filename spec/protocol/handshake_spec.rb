require 'spec_helper'

describe WebSocketRb::Protocol::Handshake do
  let(:conn) { double('conn') }
  subject { described_class.new(conn) }

  it 'has a constant MAGIC key' do
    expect(described_class::MAGIC_KEY).to eq('258EAFA5-E914-47DA-95CA-C5AB0DC85B11')
  end

  context '#calculate_key' do
    let(:key) { 'dGhlIHNhbXBsZSBub25jZQ==' }

    it 'should raise an error if argument is not a string' do
      expect { subject.send(:calculate_key, 1) }.to raise_error(ArgumentError, 'Key must be a String')
    end

    it 'should return a proper value' do
      expect(subject.send(:calculate_key, key)).to eq('s3pPLMBiTxaQ9kYGzzhZRbK+xOo=')
    end
  end
end
