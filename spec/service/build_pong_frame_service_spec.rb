require 'web_socket_rb/service/build_pong_frame_service'
require 'spec_helper'

describe WebSocketRb::Service::BuildPongFrameService do
  let(:ping_frame) { double('ping_frame') }
  subject { described_class.new(ping_frame) }

  describe '#run' do
    context 'when ping frame does not have application data' do
      before do
        allow(ping_frame).to receive(:payload_data) { nil }
      end
      it 'should return a new PONG frame' do
        expect(subject.run).to be_a(WebSocketRb::Wrapper::FramePong)
      end
      it 'should return a PONG frame with the same text as ping' do
        expect(subject.run.payload_data).to be_nil
      end
    end

    context 'when ping frame does have application data' do
      before do
        allow(ping_frame).to receive(:payload_data) { 'This is a sample text' }
      end
      it 'should return a new PONG frame' do
        expect(subject.run).to be_a(WebSocketRb::Wrapper::FramePong)
      end
      it 'should return a PONG frame with the same text as ping' do
        expect(subject.run.payload_data).to eq('This is a sample text')
      end
    end
  end
end
