require 'web_socket_rb/service/build_pong_frame_service'
require 'spec_helper'

describe WebSocketRb::Service::BuildCloseFrameService do
  let(:close_frame) { double('close_frame') }
  subject { described_class.new(close_frame) }

  describe '#run' do
    it 'should return a valid object' do
      expect(subject.run).to be_a(WebSocketRb::Wrapper::FrameClose)
      expect { subject.run.to_bytes }.to_not raise_error
    end
  end
end
